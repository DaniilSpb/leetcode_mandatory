var maxProfit = function(prices) {
    let maxDiff = prices[0];
    let size = prices.length-1;
    for(let i=0;i<size;i++){
        for(let j=i+1;j<size + 1;j++){
                if (prices[i]<prices[j] && prices[i]-prices[j] < maxDiff ){
                    maxDiff = prices[i]-prices[j];
            }
        }
    }
   if (maxDiff == prices[0]){return 0};
    return -maxDiff;
}
// timeout need algorithm improvement O(n^2)