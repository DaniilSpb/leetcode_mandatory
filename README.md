# leetcode_mandatory

### Array
- [x] Two Sum
- [ ] Best Time to Buy and Sell Stock
- [x] Contains Duplicate
- [ ] Product of Array Except Self
- [ ] Maximum Subarray
- [ ] Maximum Product Subarray
- [ ] Find Minimum in Rotated Sorted Array
- [ ] Search in Rotated Sorted Array
- [ ] 3 Sum
- [ ] Container With Most Water
---
### Binary
- [ ] Sum of Two Integers
- [ ] Number of 1 Bits
- [ ] Counting Bits
- [ ] Missing Number
- [ ] Reverse Bits
--- 
### Dynamic Programming
- [ ] Climbing Stairs
- [ ] Coin Change
- [ ] Longest Increasing Subsequence
- [ ] Longest Common Subsequence
- [ ] Word Break Problem
- [ ] Combination Sum
- [ ] House Robber
- [ ] House Robber II
- [ ] Decode Ways
- [ ] Unique Paths
- [ ] Jump Game
---
#### [Source](https://leetcode.com/discuss/general-discussion/460599/blind-75-leetcode-questions)